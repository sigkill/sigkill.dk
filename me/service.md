# Service

I sometimes volunteer for various things; either as part of my job or
because I think it's important.  Sometimes I have to provide lists of
these things, and I can never remember them, so welcome to the most
boring part of this site.

## Academic

### Chairs

* [VMCAI 2021](https://popl21.sigplan.org/home/VMCAI-2021) Artifact Evaluation chair.
* [FHPNC 2021](https://icfp21.sigplan.org/home/FHPNC-2021) chair.
* [ARRAY 2022](https://pldi22.sigplan.org/home/ARRAY-2022) chair.
* [IFL 2022](https://ifl22.github.io/) communications chair.
* [ARRAY 2023](https://pldi23.sigplan.org/home/ARRAY-2023) chair.
* [ML 2024](https://icfp24.sigplan.org/home/mlworkshop-2024) chair.

### Program committees

* [MCC 2018](https://sites.google.com/site/mccworkshop2018)
* [MCC 2019](https://sites.google.com/view/mcc2019)
* [FHPC 2018](https://icfp18.sigplan.org/track/FHPC-2018-papers)
* [ARRAY 2019](https://pldi19.sigplan.org/home/ARRAY-2019)
* [PPoPP 2023](https://conf.researchr.org/home/PPoPP-2023)
* [FProPer 2024](https://icfp24.sigplan.org/home/fproper-2024)

### External review commitees

* [ICFP 2019](https://icfp19.sigplan.org/)
* [ICFP 2020](https://icfp20.sigplan.org/)
