---
title: Notation as a Tool of Talk
description: What came first - the title wordplay or the post idea?
---

The title of is post is a pun on [Notation as a Tool of
Thought](https://www.jsoftware.com/papers/tot.htm), a famous essay
written by Ken Iverson, the creator of APL and other array languages.
I listen to various podcasts, two of which are the [Array
Cast](https://aplwiki.com/wiki/Array_Cast), which concerns itself with
array languages, and [ADSP](https://adspthepodcast.com/), which
contains an eclectic mix of programming topics, but very frequently
discusses structured parallel programming.  The Array Cast is hosted
by [Conor Hoekstra](https://research.nvidia.com/person/conor-hoekstra)
and a variety of highly skilled array programmers, while ADSP is
co-hosted by [Bryce Adelstein
Lelbach](https://github.com/brycelelbach), the current CUDA C++ core
library lead (among many other things).

The fascinating thing is that both podcasts have many episodes where
experts discuss topics with essentially the same subject - structured
parallel programming.  In the Array Cast, the discussion is conducted
in the vocabulary defined by APL, while ADSP tends to use C++ terms
mostly derived from the
["algorithms"](https://en.cppreference.com/w/cpp/algorithm) standard
library (as well as third party variants such as
[Thrust](https://github.com/NVIDIA/thrust/).  what I've anecdotally
noticed is that the programming discussion on the Array Cast tends to
be at a much higher level, simply because the concision and richness
of the APL vocabulary allows many more details to be conveyed, even in
spoken form.  Discussing code details, which is a textual medium, on a
podcast is always a dubious proposition, but the richer vocabulary of
APL clearly allows the discussion to be more focused than when using
the more clumsy vocabulary of C++, which involves far too many
irrelevant things such as iterators and overloaded operators.  It's
not about the cryptic or mnemonic APL symbols (depending on your
perspective); it's about having composable high-level concepts, and
these exist independently of how you may print them.

Let me emphasise that the reason for this difference is absolutely not
due to the different host skill levels.  Not only is Conor a common
factor across both podcasts, but Bryce is obviously an expert parallel
programmer and a fine presenter
([example](https://www.youtube.com/watch?v=aFCLmQEkPUw)), but
discussing algorithms using the C++ vocabulary is like sewing while
wearing mittens.

That said, the APL vocabulary isn't all upsides.  Quite often, the
Array Cast is derailed by discussions of obscure edge cases involving
overloaded or implicit operations
(e.g. [overtaking](https://aplwiki.com/wiki/Take#Overtaking) for a
mild example) - notational shorthands that may help with the terseness
of written code, but seems to make oral discussion difficult, as you
end up with concepts that are difficult to name.  Of course, it's
possible that *this* quirk can be blamed on the hosts.

(As a final note, let me strongly recommend listening to anything that
Conor produces - his enthusiasm for whatever he chooses to talk about
is *highly* contagious.  If you work in parallel programming and ever
struggle with motivation, just listen to one of his episodes on
[e.g. parallel C++
algorithms](https://adspthepodcast.com/2022/04/08/Episode-72.html),
and be reminded of the enthusiasm that got you into the field in the
first place.)
