---
title: Notes on teaching from a mediocre teacher
---

# Notes on teaching from a mediocre teacher

I am not a naturally talented teacher. I lack the empathy to
understand how students think and how to explain things from their
perspective. I have a tendency to speak too fast and to stutter.
[Interaction with other humans exhausts me rather
quickly.](https://en.wikipedia.org/wiki/Extraversion_and_introversion#Introversion)
I am awkward in social situations except with very close friends.
Nevertheless, as a working academic I have taught a lot, as everything
from Teaching Assistant to Course Supervisor. My teaching usually goes
well, in the sense that students provide positive feedback and seem to
learn what they are supposed to. Below you can find my notes on how I
structure my teaching.

For most of my teaching responsibilities, I have had great leeway to
change things as I saw fit. Much of the advice may not be applicable
if you are required to teach someone else's vision. I have also mostly
taught practically oriented courses (programming and systems
material), and the way a successful theoretical course should be
structured may be different.

## Teaching Assistants

At the University of Copenhagen, we can expect the university to fund
one Teaching Assistant (TA) for every 25 students. Every TA is hired
for 130 hours for a course. Utilising the TAs effectively is a core
aspect of running a successful course. It is also important to treat
your TAs well, not to overwork them, and not to assign them excessive
responsibilities (although some will ask for this): not just because
it is immoral to do so, but because word will spread, and nobody will
want to be a TA in your course next year.

## The Coarse Course structure

I structure my courses as a sequence of assignments that must be
submitted in order to qualify for the exam. Usually the assignments
are weekly, but bi-weekly can also work. The assignments are graded by
TA on a simple point scale (such as 0-4).

To qualify for the exam, each assignment must receive at least one
point, and students must receive at least half of all possible points
in the assignments. The idea is not that exam qualification should be
difficult, but rather to encourage an even workload. The simple point
scale is to avoid TAs having to spend too much time deciding on
fine-grained point decisions. This also means that you do not have to
spend much time ensuring perfect consistency between TA grades,
because there are usually plenty of points for students who follow the
course. It is common for every student who hands in every assignment
to qualify for the exam.

I usually prefer that the assignments have no impact on the final
grade (i.e., only performance on the exam affects the grade). If the
assignments influence the final grade, then you have to spend more
time ensuring that grading is consistent. This can be impractical on
larger courses.

## Structure of the assignments

Weekly assignments should not be too large and have too many degrees
to freedom, as that will also make them very time-consuming to
correct. I prefer to structure programming assignments as implementing
a program according to a very rigid provided spec (almost always with
a code handout). The students are also asked to write a *short* report
where they have to answer specific qualitative and quantitative
questions about their code. [See this
example.](https://github.com/diku-dk/hpps-e2023-pub/blob/main/assignments/a4/a4.pdf)

One technique, that has worked well for me, is for the assignments to
involve extending code developed during the exercises (see below). In
this setting, the exercises involve writing exactly the code handout
for the assignments (or as close as you can get). This avoids two
common problems.

1. Students get stuck trying to grasp a nontrivial code handout.

2. Students feel that the exercises are pointless. (In many courses
   they are.)

I tend to reuse assignments every year, usually with only minor
changes and corrections. The main advantage of this is a low error
rate, and that the assignments will not have an unpredictable
workload. The downside is that students can usually find solutions if
they look for them. However, if the assignments merely serve to
qualify for the exam, this will not have a positive influence on their
final grade, so they have an incentive not to do this.

## Lectures

Lectures, understood as a teaching situation with a single teacher in
front of a large group of students, are an inherently poor learning
environment. Due to the quantity of students, communication is
necessarily one-way. You will also rarely have a good grasp of how
well the students are following along, and individual attention is
quite impractical.

I prefer to use my lectures to demonstrate my own way of working with
the material, in order to transfer some of my own intuition to those
of my students. In my best lectures, I do not use slides at all, but
instead write programs in a text editor (shown via a projector). This
makes the ideas very concrete, as the students can see mistakes in my
coding, the limitations of my approach (and the solutions that are
perhaps the purpose of the lecture), and overall see every practical
detail of how to work with the material - including how a presumed
expert (me) uses the tools available. This is rather similar to how
mathematicians often do their entire lecture on a blackboard.

When using a projector to display slides or code, use a black (or
dark) text on a white (or bright) background. You can not generally
assume that the lighting conditions in the auditorium will make
white-on-black legible.

## Exercises and Exercise Classes

Exercises are voluntary and are not graded. They usually serve to
prepare students for solving assignments (see above). Exercises are
almost always coupled to *exercise classes*, which is classroom
teaching in smaller groups (at most 25 students, usually less)
overseen by a TA. The idea is that students work on the exercises, or
assignments, and can ask help from the TA. I trust my TAs to run their
exercise sessions as they see fit, although I am of course willing to
help if desired. I usually recommend that the exercise sessions are
strongly student-driven, with the TA only talking to the entire class
to clarify common mistakes they have noticed, or other very general
messages.

Exercises *must* have solutions, and these must be available to the
students. I also strongly suggest writing your own, rather than merely
copying from a textbook.

## Course Website

At my university we use a [learning management
system](https://en.wikipedia.org/wiki/Learning_management_system)
(LMS) based on [Canvas](https://en.wikipedia.org/wiki/Instructure), to
which all course students are automatically added. However, I find it
to be a very unpleasant environment in which to add and browse
content, as it has to be done via a clumsy browser-based UI. While I
do use the LMS for course-wide announcements, and leave the discussion
forum open for students to use, I prefer to use other platforms for
actual course material. In particular, I like to use raw GitHub
repositories - [see example
here](https://github.com/diku-dk/ap-e2024-pub). Note: *not* GitHub
Pages. There are two main reasons for this choice:

1. I can manage the content using standard tools (in my case, command
   line tools).

2. Students can use GitHub either as a normal website, *or* make use
   of Git's features to track changes and see updates.

I also usually create a [Discord](https://discord.com/) server for the
course. Although I am not a great fan of Discord myself, it is clear
that students are. In courses where students have the choice of using
the LMS discussion forum or Discord, almost everyone will use the
latter. However, since Discord is a proprietary third party service,
it would be inappropriate to *require* its use, and so it should never
be used for important announcements. I treat Discord roughly as a
persistent online exercise class, used for student questions and
discussions.

## Exam

Exams are a somewhat paradoxical element of the teaching, and I still
find it somewhat difficult to decide what makes for a good exam. While
learning can still take place during the exam, it should not introduce
new material. From my teachers' perspective, they are consequently the
least interesting and motivating part of the course. Yet, from the
students' perspective, they are perhaps the most important, because
their performance at the exam may have a real administrative impact on
their future studies.

An exam is a measurement of the students' achievement of the learning
goals. A good measurement is close to the underlying true value, which
cannot be observed directly, and in particular is resistant to noise.

I have advice for the following exam formats.

### Oral exams based on randomly drawn questions

I have limited experience here. I find that this format has
significant noise, as some students find it very stressful. The upside
of this format is that it requires very little preparation from the
teachers. A downside is that it does not scale to large courses. I
suggest only using this format for remedial re-exams and similar.

### Project presentation

In this exam form, the final couple of weeks of the course are spent
on the students doing a small project (either in groups or
individually), which is then presented orally. This is my favourite
exam form, as I find it the most realistic. The students get to do
interesting work, and you still get to quiz them on it. Because they
are presenting their own work, my experience is that it is less
stressful. The supervision of projects is very time consuming, as is
the exam itself (typically at least 20 minutes per student), so it
also does not scale to large courses. I strongly recommend it for
smaller master's courses, however.

### Take-home written exam

In this exam form, the students are handed a problem statement and
given time to work on it at home. This exam form is well suited for
programming courses, there the form of the exam problem strongly
resembles the assignments they are used to from the course. This exam
format scales well to larger courses, although you will likely need to
allocate TA hours for "pre-correction", where TAs run the code and
diagnose failures, using a test suite that *you* provide. I will
usually provide a fine-grained grading rubric with a points scheme
that the TAs fill out for every exam submission. The course teachers
(and censors) will use these rubrics to decide on a grade. The
important concerns here is that TAs may not award grades, must not be
overworked, and are not formally responsible for any part of the exam.

Typically the problem is designed for 20 hours of work. I prefer to
give students a full week to work on it, with the understanding that
they are not expected or supposed to work full-time. However, some
students still do so, and end up getting incredibly stressed and not
sleeping well for a week. The alternative is giving them only three
days, but this runs a high risk of the exam period colliding with
other activities (illness or another exam), which lowers the accuracy
of the measurement.

It is a good idea to have the exam problem ready several weeks before
it is handed out to the students, and to have your TAs perform trial
runs to detect errors or unexpectedly difficult areas.

### On-premise written exam

The traditional exam format where you put all your students in a gym
hall (or equivalent) with a problem sheet, and have them sit there for
3-4 hours. Today, these are often held in a digital version, where the
problem set is given to the students as a PDF. *Never use editable
PDFs*. These are a trap. The software support is still very spotty, so
depending on which client the students use (browser, Acrobat Reader,
Evince, Foxit, etc), your client may or may not be able to read what
they put in. There's also the risk the LMS will screw it up for some
reason. I have many horror stories, and I can't really blame the
students. Today I would always suggest handing out non-editable PDFs
and instruct the students to use
[Xournal++](https://xournalpp.github.io/) (or similar) to directly
just draw or add text on top of the PDF, then export to a
(non-editable) PDF at the end. This will be readable with any PDF
reader with high probability.
