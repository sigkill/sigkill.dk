---
title: Various Nix things
---

# Various Nix things

## Using `nix-shell` relative to a different package set

Example, using GHC 9.8.1 from a PR before it was merged into Nixpkgs
proper.

```
nix-shell -I nixpkgs=https://github.com/vaibhavsagar/nixpkgs/archive/refs/heads/vaibhavsagar/ghc981.zip -p haskell.compiler.ghc981
```

Note that this is equivalent to running random code off the internet,
and that the above URL is not pinned at all.

## Using ad-hoc packages from PyPI in `nix-shell`

Put something like this in your `shell.nix`:

```
{ pkgs ? import <nixpkgs> {} }:
let
  my-python-packages = ps: with ps; [
    (buildPythonPackage rec {
      pname = "futhark-data";
      version = "1.0.1";
      src = fetchPypi {
        inherit pname version;
        sha256 = "sha256-UJ0x642f5Q9749ffYIimQuF+sMTVOyx4iYZgrB86HFo=";
      };
      doCheck = false;
    })
    numpy
    scipy
  ];
  my-python = pkgs.python3.withPackages my-python-packages;
in my-python.env
```

[Taken from here.](https://nixos.wiki/wiki/Python)

## A minimal `shell.nix`

It is minimal in that the file is small, not that the environment is small.

```
{ pkgs ? import <nixpkgs> {} }:
pkgs.stdenv.mkDerivation {
  name = "shell";
  buildInputs =
    with pkgs;
    [
      # add packages here
    ];
}
```
