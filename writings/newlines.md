---
title: Programming Languages and Newlines
---

# List of programming languages that require newlines

A *oneliner* is a computer program written in a single line, without
using any newline (`\n`) characters. This is common in interactive
shell scripting, which for practical reasons are only written as a
single line, and languages such as Perl and APL have a culture of
writing substantial pieces of logic as single lines.

Most languages whose syntax is based on standard tokenisation, where
all forms of whitespace are treated similarly, allow us to replace any
newline character with a space character, and thus let us write
programs of any complexity on a single line. Even languages that use
newline characters rather integrally, such as Python, may have escape
hatch notation such as [semicolons](semicolons.html) that allows us to
avoid newlines in most or all cases.

The following lists attempts to catalogue programming languages that
do *not* allow arbitrarily complicated programs to be written on a
single line. Sometimes the reason is particularly subtle (or not even
clear), which are of course the most interesting cases.

Some language specifications (remember when languages didn't just have
implementations?) allow implementations to impose limits on line
lengths, which naturally puts a limit on the size of single line
programs. We are ignoring those limits in the following, since it's
not terribly interesting.

Further, the POSIX standard defines a *line* as always ending in `\n`,
even the last (or only) line in a file ([POSIX 2017, section
3.206](https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/)).
This also precludes `\n`-free programming. We will leave it to the
Unix philosophers whether a *oneliner* may have a single newline in
it. Similarly, POSIX also allows lines to have a maximum length.

## The list

* **C and C++**. Most of C's syntax does not distinguish newlines from
  other kinds of whitespace. However, in order to perform any kind of
  I/O, you must `#include` header files, and an `#include` (or any
  other preprocessor directive) must be followed by a newline. Also,
  the C99 specification requires (in section 5.1.1.2/1) that a source
  file must end in a newline. This was changed in C11 to be
  implementation-defined.

* **Fixed format FORTRAN**. Old versions of FORTRAN had a very rigid
  line-oriented syntax (even the use of specific columns were
  meaningful), hailing from its punch card origins.

* **Fixed format COBOL**, similarly to FORTRAN.

* Makefiles use a line-based syntax where a newline character
  marks the end of a statement.

* [**Whitespace**](https://esolangs.org/wiki/Whitespace), as newlines
  are used for I/O and control flow.

## The co-list

These languages may look like they require newlines, but actually do
not, for reasons that are subtle and/or interesting.

* **Python**. Statements in Python are normally separated by newlines,
  but semicolons can be used to put multiple statements on the same
  line. Some built-in bits of syntax (such as control flow) do not
  allow this, but you can work around it with recursion. The `import`
  syntax also requires a newline, but you can import modules by
  directly calling functions instead. This will certainly not look
  like very idiomatic Python.

* **Elixir** provides a lot of syntactic sugar with delimited blocks
  that requires newline characters, but it [desugars
  to](https://hexdocs.pm/elixir/1.17.3/optional-syntax.html) method
  calls and anonymous functions, which you could in principle write
  directly.

* **Haskell** is usually written with syntactically significant
  whitespace, including newlines, but this is in fact [syntactic
  sugar](https://www.haskell.org/onlinereport/haskell2010/haskellch10.html)
  over an explicit syntax defined in terms of semicolons and [curly
  braces](braces.html). Even if this was not the case, Haskell
  actually allows formfeeds and carriage returns in lieu of `\n`.
